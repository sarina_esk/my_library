#ifndef CLERK_H
#define CLERK_H
#include<iostream>
#include"book.h"
#include"student.h"
#define size 5
using namespace std;
class Clerk
{
private:
    Student  st[size];
    Book     bo[size];

public:
    void run();
    void show();
    int menu();
    int endSt();
    int endBo();
    void search();
    void reportStudent();
    void reportBook();
    void borrow();
    void reportBorrow();



};

#endif // CLERK_H
