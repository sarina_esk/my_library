TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        main.cpp \
    student.cpp \
    book.cpp \
    clerk.cpp

HEADERS += \
    student.h \
    book.h \
    clerk.h
