#ifndef BOOK_H
#define BOOK_H
#include<iostream>
#include<string>
#define size 5
using namespace std;
class Book
{
private:
string book_name;
string author_name;

int publish_day;
int publish_month;
int publish_year;

public:
    string borrow_book;
    void enterBorrowbook();
    string getBorrowbook();
    void printBorrowbook();
    Book();
    void enterBook();
    string getBook();
    string getAuthor();
    void printBook();

    int getPuday();
    int getPumonth();
    int getPuyear();


};

#endif // BOOK_H
