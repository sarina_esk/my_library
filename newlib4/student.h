#ifndef STUDENT_H
#define STUDENT_H
#include<iostream>
#include<string>
#define size 5
using namespace std;
class Student
{

private:
    string student_name;
    int id;
    int day_borrow;
    int month_borrow;
    int year_borrow;


    public:
    string student_borrow;
    void enterStudentborrow();
    string getStudentborrow();
    void printStudentborrow();
    Student();
    void enterStudent();
    string getStudent();
    int getId();

    int getDay();
    int getMonth();
    int getYear();


    void printStudent();


};

#endif // STUDENT_H
